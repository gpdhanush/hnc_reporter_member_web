import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {apiUrl} from '../global/UrlConfig';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private http: HttpClient) { }

  getAllStatus(id?): Observable<any> {
    return this.http.get<any>(id === undefined ? apiUrl.BASE_API_URL + 'allStatus.php'
      : apiUrl.BASE_API_URL + 'allStatus.php?id=' + id)
      .pipe(map(e => {
          return e;
        })
      );
  }
  getAllStates(id?): Observable<any> {
    return this.http.get<any>(id === undefined ? apiUrl.BASE_API_URL + 'allStateLists.php'
      : apiUrl.BASE_API_URL + 'allStateLists.php?id=' + id)
      .pipe(map(e => {
          return e;
        })
      );
  }
  getAllCities(id): Observable<any> {
    return this.http.get<any>(apiUrl.BASE_API_URL + 'allCityBasedState.php?id=' + id).pipe(
      map(e => {
        return e;
      })
    );
  }
  getAllTaluks(id): Observable<any> {
    return this.http.get<any>(apiUrl.BASE_API_URL + 'allTalukBasedCity.php?id=' + id).pipe(
      map(e => {
        return e;
      })
    );
  }
  getAllRelationship(id?): Observable<any> {
    return this.http.get<any>(id === undefined ? apiUrl.BASE_API_URL + 'allRelationshipLists.php'
      : apiUrl.BASE_API_URL + 'allRelationshipLists.php?id=' + id)
      .pipe(map(e => {
          return e;
        })
      );
  }
  getAllDesignation(): Observable<any> {
    return this.http.get<any>(apiUrl.BASE_API_URL + 'getAllDesignation.php').pipe(
      map(e => {
        return e;
      })
    );
  }
}
