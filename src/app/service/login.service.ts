import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {apiUrl} from '../global/UrlConfig';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  login(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'login.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
}
