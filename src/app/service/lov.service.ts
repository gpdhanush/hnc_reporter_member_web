import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {apiUrl} from '../global/UrlConfig';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LovService {

  constructor(private http: HttpClient) { }
  saveState(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveStates.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
  saveDistrict(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveDistrict.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
  saveTaluk(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveTaluk.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
  saveStatus(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveStatus.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
  saveDesignation(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveDesignation.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
  saveRelationship(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveRelationship.php', JSON.stringify(params)).pipe(
      map(e => {
        return e;
      })
    );
  }
  loadAllDistricts(id?): Observable<any> {
    return this.http.get<any>(id === undefined ? apiUrl.BASE_API_URL + 'getAllDistrictForCityMaster.php'
      : apiUrl.BASE_API_URL + 'getAllDistrictForCityMaster.php?id=' + id)
      .pipe(map(e => {
          return e;
        })
      );
  }
  loadAllTalukLists(id?): Observable<any> {
    return this.http.get<any>(id === undefined ? apiUrl.BASE_API_URL + 'getAllTaluksForTalukMaster.php'
      : apiUrl.BASE_API_URL + 'getAllTaluksForTalukMaster.php?id=' + id)
      .pipe(map(e => {
          return e;
        })
      );
  }
}
