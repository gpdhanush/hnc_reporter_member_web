import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {apiUrl} from '../global/UrlConfig';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportersService {

  constructor(private http: HttpClient) { }
  saveReporters(params): Observable<any> {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'saveReportersMaster.php', params).pipe(
      map(e => {
        return e;
      })
    );
  }
  loadReporters(id?): Observable<any> {
    return this.http.get<any>(id === undefined ? apiUrl.BASE_API_URL + 'getMemberList.php' :
      apiUrl.BASE_API_URL + 'getMemberList.php?id=' + id).pipe(
      map(data => {
        return data;
      })
    );
  }
  uploadProfile(image): any {
    return this.http.post<any>(apiUrl.BASE_API_URL + 'uploadProfile.php', image);
  }
}
