import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FullComponent} from './components/layout/full/full.component';
import {EmptyComponent} from './components/layout/empty/empty.component';

const routes: Routes = [
  {
    path: '',
    component: EmptyComponent,
    children: [
      { path: '', redirectTo: 'user', pathMatch: 'full'},
      { path: 'user',
        loadChildren: () => import('./modules/user-master/user.module').then(
          m => m.UserModule
        )
      },
    ]
  },
  {
    path: '',
    component: FullComponent,
    children: [
      { path: 'dashboard',
        loadChildren: () => import('./modules/dahboard/dashboard.module').then(
          m => m.DashboardModule
        )
      },
      { path: 'reporters',
        loadChildren: () => import('./modules/reporters/reporters.module').then(
          m => m.ReportersModule
        )
      },
      { path: 'lov-master',
        loadChildren: () => import('./modules/lov-master/lov-master.module').then(
          m => m.LovMasterModule
        )
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
