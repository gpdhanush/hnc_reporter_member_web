import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportersComponent} from './reporters/reporters.component';
import {NewComponent} from './new/new.component';


const routes: Routes = [
  // { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: '', component: ReportersComponent },
  { path: 'new', component: NewComponent },
  { path: 'update/:id', component: NewComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportersRoutingModule { }
