import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportersRoutingModule } from './reporters-routing.module';
import { ReportersComponent } from './reporters/reporters.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelect2Module} from 'ng-select2';
import { NewComponent } from './new/new.component';
import {DataTablesModule} from 'angular-datatables';
import {NgxSpinnerModule} from 'ngx-spinner';

@NgModule({
  declarations: [ReportersComponent, NewComponent],
  imports: [
    CommonModule,
    ReportersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelect2Module,
    DataTablesModule,
    NgxSpinnerModule
  ]
})
export class ReportersModule { }
