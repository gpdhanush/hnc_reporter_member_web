import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import Inputmask from 'inputmask';
import {GlobalService} from '../../../service/global.service';
import {ReportersService} from '../../../service/reporters.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import Swal from 'sweetalert2';
declare var $;

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  reportersForm: FormGroup;
  maskArray = ['dateOfBirth', 'nomineeDob', 'joiningDate', 'expireDate', 'nextRenewalDate'];
  stateLists: any;
  citiesList: any;
  talukLists: any;
  statusList: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  cropScreen = false;
  relationShip: any;
  designationList: any;
  fbutton = 'Save';
  profilePicture = '';
  imageSrc: string;
  urlId: any;
  profileForm: FormGroup;
  stateLevel = [
    {id: 'Y', text: 'Yes'},
    {id: 'N', text: 'No'},
  ];
  uploadProfile = false;

  constructor(private fb: FormBuilder, private global: GlobalService, private reporters: ReportersService,
              public router: Router, private spinner: NgxSpinnerService, private activeRouter: ActivatedRoute) { }

  ngOnInit(): void {
    $('.dropify').dropify({
      messages: {
        default: '',
        replace: 'click to replace',
        remove:  'Remove',
        error:   'Oops...'
      }
    });
    window.dispatchEvent( new Event( 'resize' ) );
    this.createForm();
    this.loadStates();
    this.loadStatus();
    this.loadRelationship();
    this.loadDesignation();
    this.maskActivations();
    this.urlId = this.activeRouter.snapshot.paramMap.get('id');
    if (this.urlId) {
      this.loadMemberList(this.urlId);
      this.fbutton = 'Update';
    }
    this.profileForm = this.fb.group({
      avatar: ['']
    });
  }
  loadMemberList(id): any {
    this.spinner.show();
    this.reporters.loadReporters(id).subscribe(data => {
      if (data.responseType === 'S') {
        const val = data.responseValue[0];
        this.loadCities(val.state);
        this.profilePicture = val.profileUrl;
        this.reportersForm.patchValue({
          reporterId: val.id,
          channelName: val.channelName,
          stateLevel: val.stateLevel,
          empId: val.empID,
          fullName: val.fullName,
          dateOfBirth: val.dob,
          designation: val.designation,
          mobile: val.mobile,
          emailId: val.email,
          bloodGroup: val.bloodGroup,
          vehicleNo: val.vehicleNo,
          aadhaarNo: val.aadhaarNo,
          drivingLicence: val.drivingNo,
          streetAddress1: val.street1,
          streetAddress2: val.street2,
          state: parseInt(val.state, 10),
          city: parseInt(val.city, 10),
          taluk: val.taluk,
          pinCode: val.pincode,
          joiningDate: val.joinDate,
          expireDate: val.expireDate,
          nextRenewalDate: val.renewalDate,
          status: val.status,
          nomineeName: val.nomineeName,
          nomineeRelationship: val.nomineeRelation,
          nomineeDob: val.nomineeDob,
          nomineeAddress: val.nomineeAddress,
          profileUrl: val.profileUrl
        });
        this.spinner.hide();
      }
    }, e => {
      this.spinner.hide();
    });
  }
  maskActivations(): any {
    // MASK INITIALIZATION
    this.maskArray.forEach(d => {
      const selector = document.getElementById(d);
      const im = new Inputmask('dd-mm-yyyy', { placeholder: 'DD-MM-YYYY' });
      im.mask(selector);
    });
    const aadhaar = document.getElementById('aadhaarNo');
    const no = new Inputmask('9999 9999 9999', {jitMasking: false, showMaskOnFocus: false, showMaskOnHover: false, });
    no.mask(aadhaar);
    const pinCode = document.getElementById('pinCode');
    const pin = new Inputmask('999999', {jitMasking: false, showMaskOnFocus: false, showMaskOnHover: false, });
    pin.mask(pinCode);
    const mobile = document.getElementById('mobile');
    const mbl = new Inputmask('9999999999', {jitMasking: false, showMaskOnFocus: false, showMaskOnHover: false, });
    mbl.mask(mobile);
  }
  get f(): any{
    return this.reportersForm.controls;
  }
  createForm(): any {
    this.reportersForm = this.fb.group({
      reporterId: new FormControl(''),
      channelName: new FormControl('', Validators.compose([Validators.required])),
      empId: new FormControl('', Validators.compose([Validators.required])),
      fullName: new FormControl('', Validators.compose([Validators.required])),
      dateOfBirth: new FormControl(''),
      designation: new FormControl(''),
      mobile: new FormControl(''),
      emailId: new FormControl(''),
      bloodGroup: new FormControl(''),
      vehicleNo: new FormControl(''),
      aadhaarNo: new FormControl(''),
      drivingLicence: new FormControl(''),
      streetAddress1: new FormControl(''),
      streetAddress2: new FormControl(''),
      state: new FormControl(''),
      city: new FormControl(''),
      taluk: new FormControl(''),
      pinCode: new FormControl(''),
      joiningDate: new FormControl(''),
      expireDate: new FormControl(''),
      nextRenewalDate: new FormControl(''),
      status: new FormControl(''),
      nomineeName: new FormControl(''),
      nomineeRelationship: new FormControl(''),
      nomineeDob: new FormControl(''),
      nomineeAddress: new FormControl(''),
      profileUrl: new FormControl(''),
      stateLevel: new FormControl('N'),
    });
  }
  loadStates(): any {
    this.global.getAllStates().subscribe(data => {
      if ( data.responseType === 'S') {
        this.stateLists = data.responseValue;
      }
    }, e => {
      this.stateLists = [];
    });
  }
  loadStatus(): any {
    this.global.getAllStatus().subscribe(data => {
      if ( data.responseType === 'S') {
        this.statusList = data.responseValue;
      }
    }, e => {
      this.statusList = [];
    });
  }
  loadRelationship(): any {
    this.global.getAllRelationship().subscribe(data => {
      if ( data.responseType === 'S') {
        this.relationShip = data.responseValue;
      }
    }, e => {
      this.relationShip = [];
    });
  }
  loadDesignation(): any {
    this.global.getAllDesignation().subscribe(data => {
      if ( data.responseType === 'S') {
        this.designationList = data.responseValue;
      }
    }, e => {
      this.designationList = [];
    });
  }
  reportSave(value: any): any {
    this.spinner.show();
    this.reporters.saveReporters(JSON.stringify(value)).subscribe(data => {
      if ( data.responseType === 'S') {
        this.spinner.hide();
        Swal.fire({
          icon: 'success',
          title: 'Success',
          text: data.responseValue.message
        });
        this.reportersForm.reset();
        this.router.navigate(['reporters']);
      }
    }, e => {
      this.spinner.hide();
      Swal.fire(e.error.responseValue.messagem, '', 'error');
      // alert(e.error.responseValue.message);
    });
  }
  loadCities(id: any): any {
    this.global.getAllCities(id).subscribe(data => {
      if (data.responseType === 'S') {
        this.citiesList = data.responseValue;
      }
    }, e => {
      this.citiesList = [];
    });
  }
  loadTaluks(id): any {
    this.global.getAllTaluks(id).subscribe(data => {
      if (data.responseType === 'S') {
        this.talukLists = data.responseValue;
      }
    }, e => {
      this.talukLists = [];
    });
  }

  onSubmit(): any {
    this.spinner.show();
    const formData = new FormData();
    formData.append('avatar', this.profileForm.get('avatar').value);
    this.reporters.uploadProfile(formData).subscribe(
      (res) => {
        if (res.responseType === 'S') {
          this.spinner.hide();
          alert(res.responseValue.message);
          this.reportersForm.controls.profileUrl.setValue(res.responseValue.url);
          this.uploadProfile = true;
        }
      },
      (err) => {
        this.spinner.hide();
        alert(err.error.responseValue.message);
      }
    );
  }
  onFileSelect(event): any {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.profileForm.get('avatar').setValue(file);
    }
  }
}
