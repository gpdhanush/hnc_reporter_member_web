import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {ReportersService} from '../../../service/reporters.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';

@Component({
  selector: 'app-reporters',
  templateUrl: './reporters.component.html',
  styleUrls: ['./reporters.component.css']
})
export class ReportersComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  reportersMaster: any;
  currentReporter: any;

  constructor(private router: Router, private reporters: ReportersService,
              private spinner: NgxSpinnerService) { }
  ngOnInit(): void {
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20],
      language: {searchPlaceholder: ' Search'},
      columnDefs: [ {
        orderable: false,
        targets: [8]
      }]
    };
    this.loadMemberList();
  }
  navigate(url): any {
    this.router.navigate([url]);
  }
  loadMemberList(): any {
    this.spinner.show();
    this.reporters.loadReporters().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.reportersMaster = data.responseValue;
        this.dtTrigger.next();
      }
    }, e => {
      this.spinner.hide();
      this.reportersMaster = [];
      // this.dtTrigger.next();
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  view(rm: any): any {
    console.log(rm);
    this.currentReporter = rm;
  }

  delete(sl: any): any {
    this.spinner.show();
    const a = {
      remove: 'Y',
      reporterId: sl.id
    };
    this.reporters.saveReporters(a).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadMemberList();
      }
    });
  }
}
