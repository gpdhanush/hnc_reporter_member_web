import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LovMasterRoutingModule } from './lov-master-routing.module';
import { StateComponent } from './state/state.component';
import { DistrictsComponent } from './districts/districts.component';
import { TalukComponent } from './taluk/taluk.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelect2Module} from 'ng-select2';
import { StatusComponent } from './status/status.component';
import { DesignationComponent } from './designation/designation.component';
import { RelationshipComponent } from './relationship/relationship.component';
import {DataTablesModule} from 'angular-datatables';
import {NgxSpinnerModule} from 'ngx-spinner';


@NgModule({
  declarations: [StateComponent, DistrictsComponent, TalukComponent, StatusComponent, DesignationComponent, RelationshipComponent],
  imports: [
    CommonModule,
    LovMasterRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelect2Module,
    DataTablesModule,
    NgxSpinnerModule
  ]
})
export class LovMasterModule { }
