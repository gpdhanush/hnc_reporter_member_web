import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {GlobalService} from '../../../service/global.service';

@Component({
  selector: 'app-designation',
  templateUrl: './designation.component.html',
  styleUrls: ['./designation.component.css']
})
export class DesignationComponent implements OnInit {
  sbutton = 'Save';
  header = 'Add Status';
  designationForm: FormGroup;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  designationList: any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private lov: LovService, private global: GlobalService) { }

  ngOnInit(): void {
    this.createForm();
    this.loadDesignation();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20],
      language: {searchPlaceholder: ' Search'},
      columnDefs: [ {
        orderable: false,
        targets: [2]
      }]
    };
  }
  createForm(): any {
    this.designationForm = this.fb.group({
      designID: new FormControl(''),
      designation: new FormControl('', Validators.compose([Validators.required]))
    });
  }
  submit(value: any): any {
    this.spinner.show();
    this.lov.saveDesignation(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.designationForm.reset();
        alert(data.responseValue.message);
        this.rerender();
        this.loadDesignation();
      }
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onUpdate(id: any): any {
    this.sbutton = 'Update';
    this.header = 'Edit Designation Name';
    this.designationForm.patchValue({
      designID: id.id,
      designation: id.text,
    });
  }
  onAdd(): any {
    this.sbutton = 'Save';
    this.header = 'Add Designation Name';
    this.designationForm.reset();
  }
  remove(sl: any): any {
    this.spinner.show();
    sl.remove = 'Y';
    sl.designID = sl.id;
    sl.designation = sl.text;
    delete sl.id;
    delete sl.text;
    this.lov.saveDesignation(sl).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadDesignation();
      }
    });
  }
  loadDesignation(): any {
    this.spinner.show();
    this.global.getAllDesignation().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.designationList = data.responseValue;
        this.dtTrigger.next();
      }
    }, e => {
      this.spinner.hide();
      this.designationList = [];
    });
  }
}
