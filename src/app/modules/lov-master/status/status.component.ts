import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {GlobalService} from '../../../service/global.service';

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.css']
})
export class StatusComponent implements OnInit {
  sbutton = 'Save';
  header = 'Add Status';
  statusForm: FormGroup;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  statusList: any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private lov: LovService, private global: GlobalService) { }

  ngOnInit(): void {
    this.createForm();
    this.loadStatusMaster();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20],
      language: {searchPlaceholder: ' Search'},
      columnDefs: [ {
        orderable: false,
        targets: [2]
      }]
    };
  }
  createForm(): any {
    this.statusForm = this.fb.group({
      statusId: new FormControl(''),
      statusName: new FormControl('', Validators.compose([Validators.required]))
    });
  }
  loadStatusMaster(): any {
    this.spinner.show();
    this.global.getAllStatus().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.statusList = data.responseValue;
        this.dtTrigger.next();
      }
    }, e => {
      this.spinner.hide();
      this.statusList = [];
    });
  }
  submit(value: any): any {
    this.spinner.show();
    this.lov.saveStatus(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.statusForm.reset();
        alert(data.responseValue.message);
        this.rerender();
        this.loadStatusMaster();
      }
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onUpdate(id: any): any {
    this.sbutton = 'Update';
    this.header = 'Edit Status Name';
    this.statusForm.patchValue({
      statusId: id.id,
      statusName: id.text,
    });
  }
  onAdd(): any {
    this.sbutton = 'Save';
    this.header = 'Add Status Name';
    this.statusForm.reset();
  }
  remove(sl: any): any {
    this.spinner.show();
    sl.remove = 'Y';
    sl.statusId = sl.id;
    sl.statusName = sl.text;
    delete sl.id;
    delete sl.text;
    this.lov.saveStatus(sl).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadStatusMaster();
      }
    });
  }

}
