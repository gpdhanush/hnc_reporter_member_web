import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {GlobalService} from '../../../service/global.service';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.css']
})
export class StateComponent implements OnInit {
  sbutton = 'Save';
  header = 'Add State Name';
  stateForm: FormGroup;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  stateList: any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private lov: LovService, private global: GlobalService) { }

  ngOnInit(): void {
    this.createForm();
    this.loadStateList();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20],
      language: {searchPlaceholder: ' Search'},
      columnDefs: [ {
        orderable: false,
        targets: [2]
      }]
    };
  }
  loadStateList(): any {
    this.spinner.show();
    this.global.getAllStates().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.stateList = data.responseValue;
        this.dtTrigger.next();
      }
    }, e => {
      this.spinner.hide();
      this.stateList = [];
    });
  }
  createForm(): any {
    this.stateForm = this.fb.group({
      stateId: new FormControl(''),
      stateName: new FormControl('', Validators.compose([Validators.required]))
    });
  }
  submit(value: any): any {
    this.spinner.show();
    this.lov.saveState(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.stateForm.reset();
        alert(data.responseValue.message);
        this.rerender();
        this.loadStateList();
      }
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onUpdate(id: any): any {
    this.sbutton = 'Update';
    this.header = 'Edit State Name';
    this.stateForm.patchValue({
      stateId: id.id,
      stateName: id.text,
    });
  }
  onAdd(): any {
    this.sbutton = 'Save';
    this.header = 'Add State Name';
    this.stateForm.reset();
  }
  remove(sl: any): any {
    this.spinner.show();
    this.stateForm.patchValue({
      stateId: sl.id,
      stateName: sl.text,
    });
    sl.remove = 'Y';
    sl.stateId = sl.id;
    sl.stateName = sl.text;
    delete sl.text;
    this.lov.saveState(sl).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadStateList();
      }
    });
  }
}
