import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';
import {GlobalService} from '../../../service/global.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-taluk',
  templateUrl: './taluk.component.html',
  styleUrls: ['./taluk.component.css']
})
export class TalukComponent implements OnInit {
  sbutton = 'Save';
  header = 'Add District';
  talukForm: FormGroup;
  stateLists: any;
  citiesList: any;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  districtList: any;
  talukLists: any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private lov: LovService, private global: GlobalService) { }

  ngOnInit(): void {
    this.createForm();
    this.loadStates();
    this.loadAllTalukList();
  }
  createForm(): any {
    this.talukForm = this.fb.group({
      id: new FormControl(''),
      stateName: new FormControl('', Validators.compose([Validators.required])),
      districtName: new FormControl('', Validators.compose([Validators.required])),
      talukName: new FormControl('', Validators.compose([Validators.required])),
    });
  }
  loadStates(): any {
    this.global.getAllStates().subscribe(data => {
      if ( data.responseType === 'S') {
        this.stateLists = data.responseValue;
      }
    }, e => {
      this.stateLists = [];
    });
  }
  loadCities(id: any): any {
    this.global.getAllCities(id).subscribe(data => {
      if (data.responseType === 'S') {
        this.citiesList = data.responseValue;
      }
    }, e => {
      this.citiesList = [];
    });
  }
  submit(value: any): any {
    this.spinner.show();
    this.lov.saveTaluk(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.talukForm.reset();
        alert(data.responseValue.message);
        this.rerender();
        this.loadAllTalukList();
      }
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onUpdate(id: any): any {
    this.sbutton = 'Update';
    this.header = 'Edit Taluk Name';
    this.loadCities(id.cy_id);
    this.talukForm.patchValue({
      id: id.tk_id,
      stateName: id.st_id,
      districtName: id.cy_id,
      talukName: id.tk_name,
    });
  }
  remove(sl: any): any {
    this.spinner.show();
    const removeRow = {
      id: sl.tk_id,
      remove: 'Y'
    };
    this.lov.saveTaluk(removeRow).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadAllTalukList();
      }
    }, e => {
      this.spinner.hide();
    });
  }
  loadAllTalukList(): any {
    this.spinner.show();
    this.lov.loadAllTalukLists().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.talukLists = data.responseValue;
        this.dtTrigger.next();
      }
    }, raj => {
      this.spinner.hide();
      this.talukLists = [];
      alert(raj.error.responseValue.message);
    });
  }
}
