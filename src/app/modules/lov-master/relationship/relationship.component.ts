import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';
import {GlobalService} from '../../../service/global.service';

@Component({
  selector: 'app-relationship',
  templateUrl: './relationship.component.html',
  styleUrls: ['./relationship.component.css']
})
export class RelationshipComponent implements OnInit {
  sbutton = 'Save';
  header = 'Add Relationship';
  relationshipForm: FormGroup;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  relationList: any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private lov: LovService, private global: GlobalService) { }

  ngOnInit(): void {
    this.createForm();
    this.loadRelationShip();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20],
      language: {searchPlaceholder: ' Search'},
      columnDefs: [ {
        orderable: false,
        targets: [2]
      }]
    };
  }
  createForm(): any {
    this.relationshipForm = this.fb.group({
      relationId: new FormControl(''),
      relationship: new FormControl('', Validators.compose([Validators.required]))
    });
  }
  submit(value: any): any {
    this.spinner.show();
    this.lov.saveRelationship(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.relationshipForm.reset();
        alert(data.responseValue.message);
        this.rerender();
        this.loadRelationShip();
      }
    });
  }
  loadRelationShip(): any {
    this.spinner.show();
    this.global.getAllRelationship().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.relationList = data.responseValue;
        this.dtTrigger.next();
      }
    }, e => {
      this.spinner.hide();
      this.relationList = [];
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onUpdate(id: any): any {
    this.sbutton = 'Update';
    this.header = 'Edit Relationship Name';
    this.relationshipForm.patchValue({
      relationId: id.id,
      relationship: id.text,
    });
  }
  onAdd(): any {
    this.sbutton = 'Save';
    this.header = 'Add Relationship Name';
    this.relationshipForm.reset();
  }
  remove(sl: any): any {
    this.spinner.show();
    sl.remove = 'Y';
    sl.relationId = sl.id;
    sl.relationship = sl.text;
    delete sl.id;
    delete sl.text;
    this.lov.saveRelationship(sl).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadRelationShip();
      }
    });
  }

}
