import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StateComponent} from './state/state.component';
import {DistrictsComponent} from './districts/districts.component';
import {TalukComponent} from './taluk/taluk.component';
import {StatusComponent} from './status/status.component';
import {DesignationComponent} from './designation/designation.component';
import {RelationshipComponent} from './relationship/relationship.component';

const routes: Routes = [
  {path: 'states', component: StateComponent},
  {path: 'districts', component: DistrictsComponent},
  {path: 'taluk', component: TalukComponent},
  {path: 'status', component: StatusComponent},
  {path: 'designation', component: DesignationComponent},
  {path: 'relationship', component: RelationshipComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LovMasterRoutingModule { }
