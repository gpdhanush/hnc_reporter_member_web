import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LovService} from '../../../service/lov.service';
import {GlobalService} from '../../../service/global.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-districts',
  templateUrl: './districts.component.html',
  styleUrls: ['./districts.component.css']
})
export class DistrictsComponent implements OnInit {
  sbutton = 'Save';
  header = 'Add District';
  districtForm: FormGroup;
  stateLists: any;
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions = {};
  dtTrigger: Subject<any> = new Subject();
  districtList: any;
  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private lov: LovService, private global: GlobalService) { }

  ngOnInit(): void {
    this.createForm();
    this.loadStates();
    this.loadAllDistricts();
    window.dispatchEvent( new Event( 'resize' ) );
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 5,
      lengthChange: true,
      lengthMenu: [5, 10, 15, 20],
      language: {searchPlaceholder: ' Search'},
      columnDefs: [ {
        orderable: false,
        targets: [3]
      }]
    };
  }
  createForm(): any {
    this.districtForm = this.fb.group({
      cityId: new FormControl(''),
      stateName: new FormControl('', Validators.compose([Validators.required])),
      districtName: new FormControl('', Validators.compose([Validators.required]))
    });
  }
  loadStates(): any {
    this.global.getAllStates().subscribe(data => {
      if ( data.responseType === 'S') {
        this.stateLists = data.responseValue;
      }
    }, e => {
      this.stateLists = [];
    });
  }
  submit(value: any): any {
    this.spinner.show();
    this.lov.saveDistrict(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.districtForm.reset();
        alert(data.responseValue.message);
        this.rerender();
        this.loadAllDistricts();
      }
    });
  }
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }
  onUpdate(id: any): any {
    this.sbutton = 'Update';
    this.header = 'Edit District Name';
    this.districtForm.patchValue({
      cityId: id.id,
      stateName: id.stateId,
      districtName: id.districtName,
    });
  }
  onAdd(): any {
    this.sbutton = 'Save';
    this.header = 'Add District Name';
    this.districtForm.reset();
  }
  remove(sl: any): any {
    this.spinner.show();
    sl.remove = 'Y';
    sl.cityId = sl.id;
    delete sl.stateId;
    delete sl.id;
    this.lov.saveDistrict(sl).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        alert(data.responseValue.message);
        this.rerender();
        this.loadAllDistricts();
      }
    }, e => {
      this.spinner.hide();
    });
  }
  loadAllDistricts(): any {
    this.spinner.show();
    this.lov.loadAllDistricts().subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        this.districtList = data.responseValue;
        this.dtTrigger.next();
      }
    }, raj => {
      this.spinner.hide();
      alert(raj.error.responseValue.message);
    });
  }
}
