import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgxSpinnerService} from 'ngx-spinner';
import {LoginService} from '../../../service/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private spinner: NgxSpinnerService,
              private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.createForm();
  }
  createForm(): any {
    this.loginForm = this.fb.group({
      userName: new FormControl('', Validators.compose([Validators.required])),
      passWord: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  loginSubmit(value: any): any {
    this.spinner.show();
    this.loginService.login(value).subscribe(data => {
      if (data.responseType === 'S') {
        this.spinner.hide();
        localStorage.setItem('currentUser', JSON.stringify(data.responseValue));
        this.router.navigate(['dashboard']);
      }
    }, e => {
      console.log(e.error);
    });
  }
}
