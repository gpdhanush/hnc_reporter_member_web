import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmptyComponent } from './components/layout/empty/empty.component';
import { FullComponent } from './components/layout/full/full.component';
import { HeaderComponent } from './components/custom-components/header/header.component';
import { FooterComponent } from './components/custom-components/footer/footer.component';
import { SidebarComponent } from './components/custom-components/sidebar/sidebar.component';
import { SettingsComponent } from './components/custom-components/settings/settings.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DataTablesModule} from 'angular-datatables';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    EmptyComponent,
    FullComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DataTablesModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
